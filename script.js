
const images = document.querySelectorAll('.image-to-show');
let currentImageIndex = 0;
let timerInterval;
let timeLeft = 3000; // 3 секунди

function showNextImage() {
    const currentImage = images[currentImageIndex];
    const nextImageIndex = (currentImageIndex + 1) % images.length;
    const nextImage = images[nextImageIndex];

    // згаснути поточне зображення
    currentImage.classList.remove('active');
    currentImage.style.opacity = 0;

    // зникає на наступному зображенні
    nextImage.classList.add('active');
    nextImage.style.opacity = 1;

    currentImageIndex = nextImageIndex;

    // скинути таймер
    timeLeft = 3000;
}

function startTimer() {
    timerInterval = setInterval(() => {
        timeLeft -= 100;
        document.querySelector('.timer').textContent = `Наступне зображення через ${timeLeft / 1000} секунди`;
        if (timeLeft <= 0) {
            showNextImage();
        }
    }, 100);
}

function stopTimer() {
    clearInterval(timerInterval);
}

function resumeTimer() {
    startTimer();
}

startTimer();

document.querySelector('#stop-btn').addEventListener('click', stopTimer);
document.querySelector('#resume-btn').addEventListener('click', resumeTimer);
